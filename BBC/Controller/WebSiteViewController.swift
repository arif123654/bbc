//
//  WebSiteViewController.swift
//  BBC
//
//  Created by apple on 2/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import WebKit

class WebSiteViewController: UIViewController,WKNavigationDelegate {
    //MARK:- Utilities
    var btnNo: Int!
    var web: WKWebView!
    var WB: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showWebsite()
        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
           web = WKWebView()
           web.navigationDelegate = self
           view = web
       }
       
       func showWebsite(){
           
           switch btnNo {
           case 1:
               WB = "https://www.bbc.com/"
           case 2:
               WB = "https://edition.cnn.com"
           case 3:
               WB = "https://www.aljazeera.com"
           default:
               print("Error")
           }
           
           
           let url = URL(string: WB)!
           web.load(URLRequest(url: url))
           web.allowsBackForwardNavigationGestures = true
       }

}
