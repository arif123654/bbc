//
//  ViewController.swift
//  BBC
//
//  Created by apple on 2/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myNewsButton: UIButton!
    @IBOutlet weak var allNewsButton: UIButton!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myNewsButton.layer.cornerRadius = 7
        self.allNewsButton.layer.cornerRadius = 7
        
        // Do any additional setup after loading the view.
    }

    @IBAction func myNewsButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "MyNewsTableViewController") as! MyNewsTableViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
   
    @IBAction func allNewsButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "AllNewsViewController") as! AllNewsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    
    
    

}

