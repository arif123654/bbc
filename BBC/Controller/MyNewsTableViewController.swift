//
//  MyNewsTableViewController.swift
//  BBC
//
//  Created by apple on 2/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import AlamofireImage
import NVActivityIndicatorView

class MyNewsTableViewController: UIViewController{
    @IBOutlet weak var newsTableView: UITableView!
   
    
        
    var model = [MyModel]()
    var newsTitle = [String]()
    var imgURL = [String]()

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsTableView.delegate = self
        newsTableView.dataSource = self
        newsTableView.separatorStyle = .none
        loadData()
        viewLoad()
        // Do any additional setup after loading the view.
    }
    

   func loadData() {
           let url = URL(string: "https://jsonplaceholder.typicode.com/photos")
           URLSession.shared.dataTask(with: url!){(data, response, error) in
               if error == nil {
                   do{
                       let myData = try!JSONDecoder().decode([MyModel].self, from: data!)
                    
                       DispatchQueue.main.async {
                        self.viewLoad()
                           for item in myData{
                            
                            self.newsTitle.append(item.title ?? "nill")
                            self.imgURL.append(item.thumbnailUrl ?? "nill")
                           }
                           self.newsTableView.reloadData()
                       }
                   }catch{
                       print(":( Nothing Found")
                   }
               }
           }.resume()
       }

    func viewLoad(){
        let load =  NVActivityIndicatorView(frame: .zero, type: .ballRotateChase, color: .blue, padding: 0)
        load.translatesAutoresizingMaskIntoConstraints = false
        super.view.addSubview(load)
        //loadingVIew.addSubview(load)
        
        NSLayoutConstraint.activate([
            load.widthAnchor.constraint(equalToConstant: 40),
            load.heightAnchor.constraint(equalToConstant: 40),
            load.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            load.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        load.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3)  {
            
            load.stopAnimating()
            
        }
    }
    
    
}



//MARK:- Tableview
extension MyNewsTableViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = newsTableView.dequeueReusableCell(withIdentifier: "cell") as! ItemsTableViewCell
        
        if let imgUrl = imgURL[indexPath.row] as? String {
            if let url = URL(string: imgUrl){
                cell.newsIMageView.af_setImage(withURL: url)
            }
        }
        cell.newsLabel.text = newsTitle[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 410
    }
}

