//
//  AllNewsViewController.swift
//  BBC
//
//  Created by apple on 2/11/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class AllNewsViewController: UIViewController {
    
    @IBOutlet weak var bbcButton: UIButton!
    @IBOutlet weak var cnnButton: UIButton!
    @IBOutlet weak var alljaButton: UIButton!
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cnnButton.layer.cornerRadius = 5
        self.cnnButton.layer.borderColor = UIColor.black.cgColor
        self.cnnButton.layer.borderWidth = 3
        self.bbcButton.layer.cornerRadius = 5
        self.bbcButton.layer.borderColor = UIColor.black.cgColor
            self.bbcButton.layer.borderWidth = 3
        self.alljaButton.layer.cornerRadius = 5
        self.alljaButton.layer.borderColor = UIColor.black.cgColor
        self.alljaButton.layer.borderWidth = 3
        
        
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func bbcButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "WebSiteViewController") as! WebSiteViewController
        vc.btnNo = 1
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    @IBAction func cnnButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "WebSiteViewController") as! WebSiteViewController
              vc.btnNo = 2
              self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    @IBAction func aljaButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "WebSiteViewController") as! WebSiteViewController
         
              vc.btnNo = 3
              self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    


}
